
# foction qui fait la Concaténation des fichiers csv 

import pandas as pd
import copy 
from datetime import datetime,date,timedelta


class Traitement_articles:
    '''
    cette classe permet de faire le prétraitement des articles à savoir : 
    --> concatener les fichiers csv, 
    -->sélectionner les articles qui parlent d'un indicateur économique précis
    '''
    def __init__(self,filenames):
        self.filenames = filenames

        
    def concat_csv(self) -> pd:
        '''
        Cette méthode  fait la concatenation des fichier csv.
        Elle prend comme arguments :
        --> une liste des noms des fichiers à concatener (filenames)  
         
        Elle retourne le fichier csv final 

        export_name doit être de la forme "nom_de_mon_fichier.csv"
        '''
        if len(self.filenames) in [0, 1] :
            return "entrer au moins deux fichiers"
        else:
            dfs = []  #liste qui va contenir les dataframes associé à chaque fichier csv 
            #boucle pour lire chaque fichier et l'ajouter à la liste des dataframe
            
            # df0 = pd.read_csv(self.filenames[0], sep="|", encoding = "utf-8", index_col = 0)
            for filename in self.filenames:
                df = pd.read_csv(filename, sep="|", encoding = "utf-8", index_col = 0)
                dfs.append(df)
            return pd.concat(dfs)


    

def extract_date(link):
    #fonction qui extrait les dates des liens 
    #exemple :
    # datetime = extract_date("https://www.lemonde.fr/economie/article/2022/01/01/pfizer-le-laboratoire-champion-du-covid-19_6107866_3234.html")
    # print(datetime) renvoi : 2022-01-01
    import re
    match = re.search(r'\d{4}/\d{2}/\d{2}',link)
    if match :
        datetime = match.group()
        datetime = datetime.replace("/", "-")
    return datetime

def df_uniforme(file_name:str, name_journal: str)->pd:
    '''
    Cette methode prend un fichier csv qui a juste 3 colonnes à savoir : |Liens|Titre|Description|Contenu
    et retourne un data frame qui contient les colonnes suivantes : |datetime|links|source|titres|intros|contenus
    '''
    
    #importation du fichier csv avec les colonnes sous la forme :|Liens|Titre|Description|Contenu
    df = pd.read_csv(file_name, sep="|", encoding = "utf-8", index_col = 0) # structure:|Liens|Titre|Description|Contenu

    
    
    # extraction des dates en applicant de la fonction extract_date à la colonne des liens + ajout de la colonne "datetime" 
    df["datetime"] = df["Liens"].apply(extract_date) # structure : |Liens|Titre|Description|Contenu|datetime|

    #On renomme les colonne de |Liens|Titre|Description|Contenu|datetime| 
    # de sorte à avoir : |links|titres|intros|contenus|datetime|
    df = df.rename(columns={"Liens":"links", "Titre":"titres", "Description":"intros"}) #structure :|links|titres|intros|contenus|datetime|

    #On va mettre |datetime| en première position, et source en 3e position

    col = df["datetime"]
    df.drop(labels="datetime", axis=1, inplace=True)
    df.insert(0,"datetime", col) # structure :datetime|links|titres|intros|contenus|

    source = [name_journal]*len(df)
    df.insert(2, "source", source)    # structure : datetime|links|source|titres|intros|contenus|
    #insertion de la colonne "date" 

    date = ["0"]*len(df)
    df.insert(0,"date", date)

    return df

def df_uniforme_Echo(file_name:str)->pd:
    '''
    Cette methode prend un fichier csv du journal Echo qui a juste les colonnes à savoir : |date|links|source|titre|description|contenu
    et retourne un data frame qui contient les colonnes suivantes : |date|datetime|links|source|titres|intros|Contenu
    '''
    #importation du fichier csv avec les colonnes sous la forme :|date|links|source|titre|description|contenu
    df = pd.read_csv(file_name, sep="|", encoding = "utf-8", index_col = 0) # |date|links|source|titre|description|contenu

    #On va renommer |date| par |datetime|, |description| par|intros| 
    df = df.rename(columns={"date":"datetime", "titre":"titres", "description":"intros", "contenu":"Contenu"}) # On obtient |datetime|links|source|titres|intros|Contenu
    
    #insertion de la colonne "date" 
    date = ["0"]*len(df)
    df.insert(0,"date", date)

    return df



def pd_to_csv(df: pd.DataFrame, file_name:str):
        '''Cette fonction permet d'exporter un dataframe vers un csv
        '''
        return df.to_csv(file_name, sep="|", encoding = "utf-8")

## permet de créer une nouvelle colonne qui vaut 1 si l'un des caractères de la liste "indicateur" est
## trouvé dans le corps de l'article.

def col_indicateur(df: pd.DataFrame, indicateur:list):
        indicateur = [ind.lower() for ind in indicateur]  
        indicateur_join="|".join(indicateur)   
       
        df["titre"]= df["titre"].str.lower()
        df["intro"]= df["intro"].str.lower()
        df["contenu"]= df["contenu"].str.lower()
        df["PIB"] = 0
        df.loc[(df["titre"].str.contains(indicateur_join)) | (df["intro"].str.contains(indicateur_join)) | (df["contenu"].str.contains(indicateur_join)), "PIB"] = 1
        return df 

#définition d'une fonction qui sélectione les articles compris entre deux dates

def select_date(date_debut, date_fin, base):
    '''
    retourne les élémentd de la base entre ces deux dates
    '''
    def str_to_date(st):
        return datetime.strptime(st, '%Y-%m-%d').date()

    data = copy.deepcopy(base)

    data_debut = datetime.strptime(date_debut, '%Y-%m-%d').date()
    data_fin = datetime.strptime(date_fin, '%Y-%m-%d').date()

    data["datetime"]=data["datetime"].apply(str_to_date)

    data_return = data.loc[(data_debut<=data["datetime"]) & (data["datetime"] <=data_fin)]
    return data_return






'''
def col_indicateur(df: pd.DataFrame, list_indicateurs):
    
    
    """cette fonction permet de dire si un article contient une thématique ou pas 
    Elle met 1 à un article s'il contient l'indicateur et 0 si non

    Args:
        df (pd.DataFrame): _data frame qui contient les articles et les liens, les contenus _
        list_indicateur (str): _ Liste de strings qui contiennent le nom ou le label de l'indicateur en question ; PIB, taux d'intérêt  _
        
        
        La liste d'indicateurs doit contenir plusieurs façons d'écrire le même indicateur
    """
    
    # on rentre un indicateur et on le transforme en minuscules
    for i in range(len(list_indicateurs)):
        list_indicateurs[i] = list_indicateurs[i].lower()
    # on 
    
    result = df.copy()
    # on mettra tout le contenu en minuscule
    result["titres"]= df["titres"].str.lower()
    result["intros"]= df["intros"].str.lower()
    result["contenus"]= df["contenus"].str.lower()
    
    
    # on initialise une colonne d'indicateur à 0
    # on donne le nom au premier élément de la liste
    result[list_indicateurs[0]] = 0
    
    # s'il existe un article dans lequel on retrouve un mot de la liste d'indicateurs, on lui attribue 1
    
    #cette variable peremttra de dire si on a trouvé au moins un mot rentré en dans l'un des textes
    bool_content = False
    for indicateur_lower in list_indicateurs:
        if ((result["titres"].str.contains(indicateur_lower)) | (result["intros"].str.contains(indicateur_lower)) | (result["contenus"].str.contains(indicateur_lower))):
            
    result.loc[(result["titres"].str.contains( indicateur_lower)) | (result["intros"].str.contains(indicateur_lower)) | (result["contenus"].str.contains(indicateur_lower)), ] = 1
    
    
    result.loc[(result["titres"].str.contains( indicateur_lower)) | (result["intros"].str.contains(indicateur_lower)) | (result["contenus"].str.contains(indicateur_lower)), ] = 1

    
    s[s.str.contains('|'.join(searchfor))]
    
    return ( result)


'''


