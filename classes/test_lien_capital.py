# ce code nous permettra d'obtenir tous les liens de l'article capital
import requests
import bs4
from bs4 import BeautifulSoup
import time
import pandas as pd
from urllib.error import HTTPError
from urllib.request import urlopen
from datetime import date
from datetime import datetime
from datetime import timedelta
import requests  # the following imports are common web scraping bundle

from urllib.error import HTTPError
from collections import defaultdict
from urllib.error import URLError
from tqdm import tqdm
import pandas as pd
import re
from requests.auth import HTTPBasicAuth
import urllib3
from pyquery import PyQuery
import webbrowser
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By



driver = webdriver.Firefox()

#voici la fonction qui permet de retourner le contenu d'un article du Capital étant donné le lien de celui -ci


def get_contenu_link_capital(url):
    driver.get(url)
    # on crée la liste de trois éléments qui sont le titre, la description et le contenu total
    full_content = list(range(3))
    
    # Nous récupérons le titre de l'article
    try:
        titre_article = driver.find_elements(By.TAG_NAME , "h1")
        titre_total = ''
        for title in titre_article:
            titre_total += title.text.strip()
        full_content[0] = titre_total
    except:
        pass
    
    
    # Ici, nous récupérons la description
    try : 
        description = driver.find_elements(By.CLASS_NAME, "article-intro")
        description_totale = ''
        for desc in description:
            description_totale += desc.text.strip()
        full_content[1] = description_totale
    except : 
        pass
    
        
    # ici, nous récupérons les paragraphes et nous les concatenons
    try : 
        paragraphaphes = driver.find_elements(By.TAG_NAME, 'p')
        text_total = ''
        for paragraphe in paragraphaphes:
            text_total += paragraphe.text.strip()
        full_content[2] = text_total
    except : 
        pass
    
    return full_content


# cette fonction servira à webscrapper un article qui provient de la rubrique
#économie et marchés de capital

def get_contenu_link_capital_entreprises(url):
    driver.get(url)
    # on crée la liste de trois éléments qui sont le titre, la description et le contenu total
    full_content = list(range(3))
    
    # Nous récupérons le titre de l'article
    try:
        titre_article = driver.find_elements(By.TAG_NAME , "h1")
        titre_total = ''
        for title in titre_article:
            titre_total += title.text.strip()
        full_content[0] = titre_total
    except:
        pass
    
    # il n' ya pas de description
    # Ici, nous récupérons la description
    try : 
        description = driver.find_elements(By.CLASS_NAME, "article-intro")
        description_totale = ''
        for desc in description:
            description_totale += desc.text.strip()
        full_content[1] = description_totale
    except : 
        pass
        
    # ici, nous récupérons les paragraphes et nous les concatenons
    try : 
        paragraphaphes = driver.find_elements(By.TAG_NAME, 'p')
        text_total = ''
        for paragraphe in paragraphaphes:
            text_total += paragraphe.text.strip()
        full_content[2] = text_total
    except :
        pass
    
    return full_content

# Ainsi, nous mettons dans la liste tous les articles de Capital
# on importe la data frame de lien 

# Date / Link /  Title / Description / Content / article
# premier lien


df_total = pd.read_csv(r'C:\Users\yanni\OneDrive\Documents\ENSAI\PFE IFRS9\web_scrapping_part\pfe-scraping\Liens_Capital_2020_2022.csv', delimiter= '|', encoding='utf-8')


df_full_content = pd.DataFrame(columns=["date","datetime", "links","source", "titre", "description", "dontenu"])
df_full_content["links"] = df_total["links"]
df_full_content["date"] = df_total["date"]
df_full_content["datetime"] = df_total["datetime"]
df_full_content["source"] = df_total["source"]

T,D,C = [],[],[]


# Ainsi, pour chaque article dans la liste de tous les articles, nous allons retourner la liste de son contenu

# on va supprimer l'article 766 qui contient une article de M bappé

for article in df_full_content["links"].tolist():
    try:
        contenu_article = get_contenu_link_capital_entreprises(article)
        T.append(contenu_article[0])
        D.append(contenu_article[1])
        C.append(contenu_article[2])
    except : 
        pass
    
df_full_content["titre"] = T
df_full_content["description"] = D
df_full_content["contenu"]= C

df_full_content_1 = df_full_content.drop("dontenu", axis= 1)

df_full_content_1.to_csv("Capital_2020_2022.csv", sep='|' , encoding="UTF-8")

df_full_content_1


# Nous allons webscrapper la deuxième rubrique de ce journal 

df_2022_entreprises = pd.read_csv(r'C:\Users\yanni\OneDrive\Documents\ENSAI\PFE IFRS9\web_scrapping_part\pfe-scraping\Liens_Capital_entreprise_2022.csv', delimiter= '|', encoding='utf-8')


df_full_content_entreprise = pd.DataFrame(columns=["date", "links","source", "titre", "description", "contenu"])
df_full_content_entreprise["links"] = df_2022_entreprises["links"]
df_full_content_entreprise["source"] = df_2022_entreprises["source"]
df_full_content_entreprise["date"] = df_2022_entreprises["datetime"]


df_full_content_entreprise
T,D,C = [],[],[]

for article in df_2022_entreprises["links"].tolist()[421:600] :
    contenu_article = get_contenu_link_capital_entreprises(article)
    T.append(contenu_article[0])
#   D.append(contenu_article[1])
    C.append(contenu_article[1])
    
df_full_content_entreprise["titre"][304:420] = T
df_full_content_entreprise["contenu"][304:420] = C





df_2021_entreprises = pd.read_csv(r'C:\Users\yanni\OneDrive\Documents\ENSAI\PFE IFRS9\web_scrapping_part\pfe-scraping\Liens_Capital_entreprise_2021.csv', delimiter= '|', encoding='utf-8')


df_full_content_entreprise = pd.DataFrame(columns=["date", "links","source", "titre", "description", "contenu"])
df_full_content_entreprise["links"] = df_2021_entreprises["links"]
df_full_content_entreprise["source"] = df_2021_entreprises["source"]
df_full_content_entreprise["date"] = df_2021_entreprises["datetime"]


df_full_content_entreprise
T,D,C = [],[],[]

for article in df_2021_entreprises["links"].tolist()[100:500] :
    contenu_article = get_contenu_link_capital(article)
    T.append(contenu_article[0])
    D.append(contenu_article[1])
    C.append(contenu_article[2])

df_full_content_entreprise["titre"][100:500] = T
df_full_content_entreprise["description"][100:500] = D
df_full_content_entreprise["contenu"][100:500] = C








deja_df_entreprises = df_full_content_entreprise

deja_df_entreprises.to_csv("test_articles_Capital_entreprise_2021.csv", sep='|' , encoding="UTF-8")



























# Ainsi, pour chaque article dans la liste de tous les articles, nous allons retourner la liste de son contenu

deja_df = df_full_content
len(D)
deja_df.to_csv("test_articles_Capital_767_1134.csv", sep='|' , encoding="UTF-8")

