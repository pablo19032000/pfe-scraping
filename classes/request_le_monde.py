from datetime import date
from datetime import timedelta
import pandas as pd
from urllib.error import HTTPError
from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup
import requests
import webbrowser
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from classes.useful_functions import get_contenu_link
from classes.useful_functions import extract_theme
import os


""" username = "yannickkonan0807@gmail.com"
password = "eXxTDsAjiNB2pEc"
# page afin de s'authentifier via un connecteur
# maintenant, nous allons effectuer toutes les requètes données.
url_authentification = "https://secure.lemonde.fr/sfuser/connexion"
driver = webdriver.Firefox()
time.sleep(10)
driver.get(url_authentification)
u = driver.find_element("name",'email')
u.send_keys(username)
p = driver.find_element('name', "password")
p.send_keys(password)
p.send_keys(Keys.RETURN)"""

'''
headers = {"Accept-Language": "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3", 
           "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:108.0) Gecko/20100101 Firefox/108.0",
           "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8"}
           
journal="https://www.lemonde.fr/"            

with open("urls.txt","w") as file:
    for l in links:
       file.write(l+"\n")
'''

# nous créeons à présent la classe le monde qui permettra de sélectionner les articles à partir d'une date de fin de 
# d'un horizon donné.


#obtenir les informations de chaque article

class LeMondeArticle():
    """ cette classe permettre d'extraire les articles du journal le monde"""
    def __init__(self,
                 year_limit_right:str, 
                 month_limit_right:str, 
                 day_limit_right:str,
                 horizon:int ):
        # on formera une date avec les infos données
        # la date limite représente la limite droite de la période de temps
        self.year_limit_right = year_limit_right
        self.month_limit_right = month_limit_right
        self.day_limit_right = day_limit_right

        
        
        # cela permet d'afficher la date au format "1987-03-04"
        # l'horizon représente le nombre de jour en arrière considéré; un horizon de 1 correspond à un jour en arrière
        # alors qu'un horizon de 7 correspond à une semaine en arrière
        self.horizon = horizon
        
        # le contenu correspond à l'ensemble des liens sous forme de dictionnaire
        # les informations qui seront sélectionnées contiendrons la date et le lien de l'article
    
    
        
    def get_archives_links(self):
        #cette méthode nous permettra de considérer les liens des articles compris entre les deux dates
        # tout d'abord, on crée les archives à ces dates.
        # nous créons les dates 
        date_limit_right = date(year= self.year_limit_right , 
                                month= self.month_limit_right,
                                day= self.day_limit_right)
        date_limit_left = date_limit_right - timedelta(days= self.horizon)
        list_of_dates = pd.date_range(date_limit_left, date_limit_right, freq= 'D').strftime('%d-%m-%Y').tolist()
        archives_links = []
        # list of dates contient les listes des dates au format str
        for date_vrai in list_of_dates:
            #archives_links[date_vrai] = ["https://www.lemonde.fr/archives-du-monde/" + date_vrai + "/"]
            lien_article = "https://www.lemonde.fr/archives-du-monde/" + date_vrai + "/"
            archives_links.append (lien_article)
        return archives_links
        
        
    def get_articles_links(self):
        # Dans cette méthode, il sera question d'extraire d'abord tous les liens aux dates données
        # puis de ne garder que ceux dont le thème est 'economie'
        # D'abord, on sélectionne tous les liens dans l'intervalle de temps donné
        links = []
        for link in self.get_archives_links() :
            try:
                html = urlopen(link)
            except HTTPError as e:
                print("url not valid", link)
            else:
                soup = BeautifulSoup(html, "html.parser")
                news = soup.find_all(class_="teaser")
                for item in news:
                    l_article = item.find('a')['href']
                    if 'en-direct' not in l_article and extract_theme(l_article) == 'economie':
                        links.append(l_article)
        return links
        # une fois obtenus les liens des articles, nous allons obtenir leurs contenus.
        # Ensuite, nous les stockerons dans une data frame
        # Il s'agit à présent de retourner la data frame du contenu total pour les dates données 
        # Nous allons définir une méthode qui permet de retourner le contenu d'un lien
        
    def get_contenu_articles(self):
        username = "yannickkonan0807@gmail.com"
        password = "eXxTDsAjiNB2pEc"
        # page afin de s'authentifier via un connecteur
        # maintenant, nous allons effectuer toutes les requètes données.
        """url_authentification = "https://secure.lemonde.fr/sfuser/connexion"
        driver = webdriver.Firefox()
        time.sleep(10)
        driver.get(url_authentification)
        u = driver.find_element("name",'email')
        u.send_keys(username)
        p = driver.find_element('name', "password")
        p.send_keys(password)
        p.send_keys(Keys.RETURN)"""
        liste_articles = self.get_articles_links()
        # A présent, nous allons accéder au contenu de chaque lien et le coller au data frame final
        # Création de la data frame finale 
        df_full_content = pd.DataFrame(columns=["Titre", "Description", "Contenu"])
        T,D,C = [],[],[]
        # Ainsi, pour chaque article dans la liste de tous les articles, nous allons retourner la liste de son contenu
        for article in liste_articles:
            contenu_article = get_contenu_link(article)
            T.append(contenu_article[0])
            D.append(contenu_article[1])
            C.append(contenu_article[2])
        df_full_content["Titre"] = T
        df_full_content["Description"] = D
        df_full_content["Contenu"] = C
        return df_full_content
        