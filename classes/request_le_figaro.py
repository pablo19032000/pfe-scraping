
import requests
from bs4 import BeautifulSoup
from datetime import datetime,date,timedelta
import time
import pandas as pd
import random as rd

class Figaro:
    '''
    cette clase permet d'accéder aux articles économiques de "liberation" 
    qui ont été publiés à partir de "date_debut" jusqu'à "date_fin"

    les dates sont au format  AAAA-MM-JJ
    '''

    def __init__(self, date_debut:str, date_fin:str=str(date.today())) -> None:
        self.date_debut= date_debut
        self.date_fin=date_fin
        self.data=[]
        self.data_selected=[]

    def get_date_debut(self) -> str:
        return self.date_debut

    def get_date_fin(self) -> str:
        return self.date_fin

    def date_from_text(self,text:str) -> str:
        """
        retourne la date au format AAAA-MM-JJ à partir d'un texte
        exemple: 16 mars 2021 
        retourne 2021-03-16
        """ 
        mois={"janv.":"01","févr.":"02", "mars":"03" , "avr.":"04", "mai":"05", "juin":"06"
              , "juil.":"07", "août":"08", "sept.":"09", "oct.":"10", "nov.":"11", "déc.":"12" }
        annee=""
        today_year=str(date.today())[:4] 
        l=text.split(" ")
        if len(l)<3:
            annee=today_year
        else:
            annee=l[2].strip()

        return annee+"-"+mois[l[1]]+"-"+l[0]   


    def contenu_articles(self) -> pd :
        headers={'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
            'referer': 'https://www.google.com'}
        journal="https://www.lefigaro.fr"            
        url= "https://www.lefigaro.fr/economie?page="
        date_inf= datetime.strptime(self.date_debut, '%Y-%m-%d').date()
        date_sup= datetime.strptime(self.date_fin, '%Y-%m-%d').date()
        df_figaro = pd.DataFrame(columns = ["date", "datetime", "links","titres","intros","contenus"])
        
        page=1
        stop=False
        bonne_page_debut=False

        while not stop:
            links,dates,dat=[],[],[]
            T,I,C=[],[],[]    # titres, inros et contenus
            lien= ""
            if page==1:
               lien= "https://www.lefigaro.fr/economie" 
            else:
                lien=url+str(page)  
            
            reponse=requests.get(lien,headers=headers)
            
            if reponse.ok:
                print(reponse, "page=", page)
                soup=BeautifulSoup(reponse.text,'lxml')
                balise_art=soup.find_all("article") ## chaque artice est contenu dans cette balise div
                print(len(balise_art))
                if not bonne_page_debut:
                    dernier_article=balise_art[-1]
                    link_dernier_article= dernier_article.find("a")["href"]
                    res=requests.get(link_dernier_article,headers=headers)
                    t=""
                    if res.ok:
                        sp=BeautifulSoup(res.text,'lxml')
                        t= sp.find("time")["datetime"]
                        t=t[0:10]
                             
                    if   datetime.strptime(t, '%Y-%m-%d').date() <= date_sup:
                        bonne_page_debut=True
                    if bonne_page_debut:
                        page-=1        
                            
                else:

                    for art in balise_art:
                        
                        try:
                            l=art.find("a")["href"]
                            res=requests.get(l,headers=headers)
                            t=""
                            da=""
                            if res.ok:
                                sp=BeautifulSoup(res.text,'lxml')
                                t,da= sp.find("time")["datetime"], sp.find("time").text
                                t=t[0:10]
                                d_art= datetime.strptime(t, '%Y-%m-%d').date()
                                if d_art< date_inf:
                                    stop = True
                                if d_art<= date_sup and d_art>= date_inf:
                                    dat.append(str(d_art))
                                    dates.append(da.strip())
                                    links.append(l)
                                    titre=" "
                                    try :
                                        titre=sp.find("h1").text
                                    except:pass    
                                    intro=" "

                                    try :
                                        intro=soup.find("p",{"class":"fig-standfirst"}).text
                                    except:pass    
                                    cont=" "

                                    try:
                                        paragraphes=soup.find_all("p",{"class":"fig-paragraph"})
                                        for p in paragraphes:
                                            cont+=p.text
                                    except:pass        
                                    T.append(titre) 
                                    I.append(intro) 
                                    C.append(cont) 
                        except:pass
                        time.sleep(0.1*rd.randint(1,3))
                    print(len(T),len(I),len(C),len(links), len(dat), len(dates))       

                        
                if bonne_page_debut and len(dat)!=0:
                    for i in range(len(links)): 
                        p=pd.DataFrame([[dates[i],dat[i],links[i],T[i],I[i],C[i]]], columns=["date", "datetime", "links","titres","intros","contenus"])  
                        df_figaro=pd.concat(objs=[df_figaro,p], ignore_index=True)

            time.sleep(0.08*rd.randint(1,3))
            page+=1
            print(page)
       

        df_figaro["source"]=["figaro"]*len(df_figaro)
        self.data = df_figaro

        return df_figaro

    def selection_articles(self, mots_cle:list=[" "]) -> pd :
        '''
        méthode de sélection des articles dans lesquels apparaissent mes mots de 
        la liste "mot_cle".
        '''
        df=self.contenu_articles()
        nrows=df.shape[0]
        index_to_keep=[]
        df1=df.copy(deep= True)

        for i in range(nrows):
            text= df["tites"][i] + df["intros"][i] +df ["contenus"] [i]
            text=text.lower()
            for elt in mots_cle :
                elt=str.lower(elt.strip())
                if elt in text :
                    index_to_keep.append(i)
                    break 
        all_index=range(nrows)         
        index_to_remove=[i for i in all_index if i not in set(index_to_keep)]
        df1.drop([index_to_remove])  

        self.data_selected=df1  

        return df1      


