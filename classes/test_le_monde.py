import requests
from bs4 import BeautifulSoup
import time
import pandas as pd
from urllib.error import HTTPError
from urllib.request import urlopen
from datetime import date
from datetime import datetime
from datetime import timedelta
import requests  # the following imports are common web scraping bundle
from urllib.error import HTTPError
from collections import defaultdict
from urllib.error import URLError
from tqdm import tqdm
import pandas as pd
import re
from classes.useful_functions import get_contenu_link
# saisir url
# on utilise shift + enter to run code in python



url = "https://www.lemonde.fr/pixels/article/2022/12/16/twitter-elon-musk-suspend-les-comptes-de-plusieurs-journalistes-americains-qui-le-critiquent-et-provoque-un-tolle_6154641_4408996.html"
reponse = requests.get(url)
soup = BeautifulSoup(reponse.text,"html.parser")


# s://www.lemonde.fr/archives-du-monde/"


def create_archive_links(year_start, year_end, month_start, month_end, day_start, day_end):
    archive_links = {}
    for y in range(year_start, year_end + 1):
        dates = [str(d).zfill(2) + "-" + str(m).zfill(2) + "-" +
                 str(y) for m in range(month_start, month_end + 1) for d in
                 range(day_start, day_end + 1)]
        archive_links[y] = [
            "https://www.lemonde.fr/archives-du-monde/" + date + "/" for date in dates]
    return archive_links


my_archives = create_archive_links(2012,2021,1, 12, 1, 31)
my_archives
# outil de création des dates
# z fill permet d'écrire la date avec deux nombres afin de se soumettre au format de base pour la date.

dates = [str(d).zfill(2) + "-" + str(m).zfill(2) + "-" +
                 str(2022) for m in range(3, 12 + 1) for d in
                 range(8, 21 + 1)]


# maintenant, on passe à la génération des liens des articles 
session = requests.Session()

session.auth = (username, password)

url_perso = "https://www.lemonde.fr/economie/article/2021/12/31/brexit-un-an-apres-le-lent-effritement-de-l-economie-britannique_6107791_3234.html"
get_contenu_link(url_perso)


soup_test = BeautifulSoup(response.text , 'html.parser')
Paragraphs_perso = soup_test.article.find_all(("p",{"class":"article__paragraph"}), recursive=False)
Paragraphs_perso


def get_articles_links(archive_links):
    links_non_abonne = []
    for link in archive_links:
        try:
            html = urlopen(link)
        except HTTPError as e:
            print("url not valid", link)
        else:
            soup = BeautifulSoup(html, "html.parser")
            news = soup.find_all(class_="teaser")
            # condition here : if no span icon__premium (abonnes)
            for item in news:
 #               if not item.find('span', {'class': 'icon__premium'}):
                    l_article = item.find('a')['href']
                    # en-direct = video
                    if 'en-direct' not in l_article and extract_theme(l_article) == 'economie':
                        links_non_abonne.append(l_article)
    return links_non_abonne

# nous allons à présent ne garder que les articles dont le thème est économie.

# ce code permet alors d'accéder à tous les articles situés dans une archive données
# l'idée ici est de de générer le lien des articles à l'aide d'une date
resultats_articles = get_articles_links(my_archives [2021])

resultats_articles

## voici la fonction qui permet de receuillir le contenu d'un article ( d'un lien)
## nous allons receuillir 


def get_single_page(url):
    try:
        html = urlopen(url)
    except HTTPError as e:
        print("url not valid", url)
    else:
        soup = BeautifulSoup(html, "html.parser")
        text_title = soup.find('h1')
        element.replace_with(element.text.replace('\xa0', ' '))
        text_body = soup.article.find_all(["p", "h2"], recursive=False)
        return (text_title, text_body)

def get_single_page_perso(url):
    try:
        html = urlopen(url)
    except HTTPError as e:
        print("url not valid", url)
    else:
        text_title = soup.find('h1').text.replace('\xa0', ' ').strip()
        text_intro = soup.find("p",{"class":"article__desc"}).text.replace('\xa0', ' ').strip()
        text_body = ""
        Paragraphs = soup.article.find_all(("p",{"class":"article__paragraph"}), recursive=False)
        for paragraph in Paragraphs:
            text_body += paragraph.text.replace('\xa0', ' ').strip()
    return (text_title, text_intro, text_body)
# cette méthode permet d'extraire les thème de par les liens des articles
url_perso = "https://www.lemonde.fr/economie/article/2021/12/31/brexit-un-an-apres-le-lent-effritement-de-l-economie-britannique_6107791_3234.html"


get_single_page_perso(url_perso)[2]

get_single_page_perso(url_perso)
# permet d'obtenir une première page de l'article en question
get_single_page(resultats_articles[1])


# Il existe une fonction permettant de sélectionner le thème d'un article même s'il se trouve dans
# les archives de celui-ci
# Ce thème se trouve dans le lien généré par la fonction pour tous les articles sélectionnés.

# par exemple : le thème ici est international
# https://www.lemonde.fr/international/article/2021/12/31/la-france-condamne-le-lance

# Nous allons donc sélectionner tous les articles dont le thème est economie.

# par exemple, voici un article qui parle d'économie 
# https://www.lemonde.fr/economie/article/2013/01/01/madrid-prevoit-le-retour-a-la-creation-d-emplois-pour-la-fin-de-l-annee-2013_1811856_3234.html

def extract_theme(link):
    try:
        theme_text = re.findall(r'.fr/.*?/', link)[0]
    except:
        pass
    else:
        return theme_text[4:-1]

valeur = extract_theme(lien_2)[4:-1]

lien_1  = 'https://www.lemonde.fr/livres/article/2021/01/01/faire-de-la-place-dans-sa-bibliotheque-entre-creve-c-ur-et-moment-de-liberation_6065000_3260.html'
lien_2 = "https://www.lemonde.fr/international/article/2019/03/01/huawei-le-canada-lance-la-procedure-d-extradition-de-meng-wanzhou-vers-les-etats-unis_5430363_3210.html"
valeur1=  re.findall(r'.fr/.*?/', lien_2)[0][4:-1]
# valeur vaut exactement "livre"










""" mon_jour_2 = date(1987,3,14)
mon_jour = date(1987,3,14) - timedelta(days=3)

list_pers = range(mon_jour , mon_jour_2)

lesto = pd.date_range(mon_jour, mon_jour_2, freq= 'D').strftime('%Y-%m-%d').tolist()
lesto.date """


def get_contenu_link(url):
    
    driver.get(url)
    # on crée la liste de trois éléments qui sont le titre, la description et le contenu total
    full_content = list(range(3))
    
    # Nous récupérons le titre de l'article
    titre_article = driver.find_elements(By.TAG_NAME , "h1")
    titre_total = ""
    for title in titre_total:
        titre_total += title.text.strip()
                
    full_content[0] = titre_article.text.strip()
    
    
    # Ici, nous récupérons la description
    description = driver.find_elements(By.CLASS_NAME, "article__desc")
    description_totale = ""
    for desc in description:
        description_totale += desc.text.strip()
    full_content[1] = description_totale
    
        
    # ici, nous récupérons les paragraphes et nous les concatenons
    paragraphaphes = driver.find_elements(By.CLASS_NAME, 'article__paragraph')
    text_total = ""
    for paragraphe in paragraphaphes:
        text_total += paragraphe.text.strip()
    full_content[2] = text_total
    
    return full_content

get_contenu_link(url_perso)


for link in ma_date.get_articles_links():
    print(get_contenu_link(link))
    
print(ma_date.get_archives_links())