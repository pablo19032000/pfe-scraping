import requests
from bs4 import BeautifulSoup
from datetime import datetime,date,timedelta
import time
import pandas as pd
import random as rd

class Echos :
    '''
    cette clase permet d'accéder aux articles économiques de "les echos" 
    qui ont été publiés à partir de "date_debut" jusqu'à "date_fin"

    les dates sont au format  AAAA-MM-JJ
    '''

    def __init__(self, date_debut:str, date_fin:str=str(date.today())) -> None:
        self.date_debut= date_debut
        self.date_fin=date_fin
        self.data=[]
        self.data_selected=[]

    def get_date_debut(self) -> str:
        return self.date_debut

    def get_date_fin(self) -> str:
        return self.date_fin

    def date_from_text(self,text:str) -> str:
        """
        retourne la date au format AAAA-MM-JJ à partir d'un texte
        exemple: 16 mars 2021 
        retourne 2021-03-16
        """ 
        mois={"janv.":"01","févr.":"02", "mars":"03" , "avr.":"04", "mai":"05", "juin":"06"
              , "juil.":"07", "août":"08", "sept.":"09", "oct.":"10", "nov.":"11", "déc.":"12" }
        annee=""
        today_year=str(date.today())[:4] 
        d=text.split("le")[-1]
        d=d.strip()
        l=d.split(" ")
        if len(l)<=4:
            annee=today_year
        else:
            annee=l[2]

        return annee+"-"+mois[l[1]]+"-"+l[0]   


    def lien_articles(self) -> pd :
        headers={'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
            'referer': 'https://www.google.com'}
        journal="https://www.lesechos.fr"      
        url= "https://www.lesechos.fr/industrie-services?page="
        date_inf= datetime.strptime(self.date_debut, '%Y-%m-%d').date()
        date_sup= datetime.strptime(self.date_fin, '%Y-%m-%d').date()
        df_echos = pd.DataFrame(columns = ["date", "datetime", "links"])
        
        page=10
        stop=False
        bonne_page_debut=False

        while not stop:
            links,dates,dat=[],[],[]
            lien=url+str(page)
            
            reponse=requests.get(lien,headers=headers)
            
            if reponse.ok:
                soup=BeautifulSoup(reponse.text,'lxml')
                balise_div=soup.find_all("div",{"class": "sc-1vhx99f-0 bxJLXB"}) ## chaque artice est contenu dans cette balise div
                if not bonne_page_debut:
                    date_page=[]
                    for d in balise_div:
                        try:
                            bal=d.find_all("span",{"class" :"sc-1i0ieo8-0 cVdWis"})[0]
                            bal=bal.text 
                            date_page.append(self.date_from_text(bal))        

                        except:
                            pass
                            
                    for  elt in date_page:
                        if   datetime.strptime(elt, '%Y-%m-%d').date() <= date_sup:
                            bonne_page_debut=True
                    if bonne_page_debut:
                        print(bonne_page_debut)
                        page-=1        
                            
                else:

                    for div in balise_div:
                        
                        try:  
                            bal=div.find_all("span",{"class" :"sc-1i0ieo8-0 cVdWis"})[0]
                            d_art= datetime.strptime(self.date_from_text(bal.text), '%Y-%m-%d').date()
                            
                            if d_art<= date_sup and d_art>= date_inf + timedelta(days=-1):
                                dat.append(str(d_art))
                                d=(bal.text).split("le")[-1]
                                dates.append(d.strip())
                                
                                balise_du_titre = div.find("h3")
                                balise_abonne = balise_du_titre.find_all("span",{'class':"sc-aamjrj-0 cdOJus"})
                                if len(balise_abonne) == 0:
                                    la_carte_du_titre = div.find("article").find("a",{"class":"sc-1560xb1-0 loUvLB sc-iphbbg-1 bMoVlm"})["href"]
                                    links.append(journal+ la_carte_du_titre)   
                        except:
                            pass
                if bonne_page_debut and len(dat)!=0:
                    
                    date_dernier_article= datetime.strptime(dat[-1], '%Y-%m-%d').date()
                    if date_dernier_article < date_inf :
                        stop = True
                    for i in range(len(links)): 
                        article_date=datetime.strptime(dat[i], '%Y-%m-%d').date()
                        if article_date>= date_inf:
                            p=pd.DataFrame([[dates[i],dat[i],links[i]]], columns=["date", "datetime", "links"])  
                            df_echos=pd.concat(objs=[df_echos,p], ignore_index=True)

            time.sleep(0.2*rd.randint(1,3))
            page+=1
            print(page)
       

        df_echos["source"]=["echos"]*len(df_echos)
        self.data = df_echos

        return df_echos


    def contenu_articles(self) -> pd :
        '''
        retourne le titre , l'into et le contenu des articles dont leurs liens web 
        sont dans "data"
        '''
        headers={'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
            'referer': 'https://www.google.com'}
        df= self.lien_articles()
        article_links=list(df["links"]) # tous les liens enregistrés dans la table data
        T,I,C=[],[],[]    # titres, inros et contenus
        for l in article_links:
            url=l.strip()
            reponse=requests.get(url,headers=headers)
            if reponse.ok:
                soup=BeautifulSoup(reponse.text,'lxml')
                titre=" "
                try:
                    titre=soup.find("h1").text
                except:pass
                intro=" "
                try:
                    intro=soup.find("p",{"class":"sc-14kwckt-6 sc-1ohdft1-0 fTlaQZ gFCllC"}).text
                except:pass
                cont=" "
                try:
                    paragraphes=soup.find_all("p",{"class":"sc-14kwckt-6 gPHWRV"})
                    for p in paragraphes:
                        cont+=p.text
                except :
                    pass    
                T.append(titre) 
                I.append(intro) 
                C.append(cont)

            time.sleep(0.5*rd.randint(1,4))  

        df["titres"]=T
        df["intros"]=I
        df["contenus"]=C
        self.data=df
        return df


    def selection_articles(self, mots_cle:list=[" "]) -> pd :
        '''
        méthode de sélection des articles dans lesquels apparaissent mes mots de 
        la liste "mot_cle".
        '''
        df=self.contenu_articles()
        nrows=df.shape[0]
        index_to_keep=[]
        df1=df.copy(deep= True)

        for i in range(nrows):
            text= df["tites"][i] + df["intros"][i] +df ["contenus"] [i]
            text=text.lower()
            for elt in mots_cle :
                elt=str.lower(elt.strip())
                if elt in text :
                    index_to_keep.append(i)
                    break 
        all_index=range(nrows)         
        index_to_remove=[i for i in all_index if i not in set(index_to_keep)]
        df1.drop([index_to_remove])  

        self.data_selected=df1  

        return df1      


