import requests
from bs4 import BeautifulSoup
from datetime import datetime,date,timedelta
import time as t
import pandas as pd
import random as rd


class Mediapart () :
    '''
    cette classe permet d'accéder aux articles économiques de médiapart 
    qui ont été publiés à partir de "date_limite"

    "date_limite" est au format AAAA-MM-JJ
    '''

    def __init__(self, date_debut:str, date_fin:str) -> None:
        self.date_debut= date_debut
        self.date_fin= date_fin
        self.data=[]
        self.data_selected=[]

    def get_date_debut(self) -> str:
        return self.date_debut

    def lien_articles(self) -> pd :
        headers={'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
            'referer': 'https://www.google.com'}
        journal="https://www.mediapart.fr"            
        url= "https://www.mediapart.fr/journal/economie?page="
        date_debut= datetime.strptime(self.date_debut, '%Y-%m-%d').date()
        date_fin= datetime.strptime(self.date_fin, '%Y-%m-%d').date()
        links=[]
        date=[]
        dat=[]
        i=1
        limite=True
        while limite:
            i+=1
            l=url+str(i)
            print(l)
            reponse=requests.get(l,headers=headers)
            print(reponse)
            if reponse.ok:
                
                soup=BeautifulSoup(reponse.text,'lxml')
                balise_div=soup.find_all("div",{"class":"teaser__container"})
                print(len(balise_div))
                for bal in balise_div:
                    try:
                        time=bal.find("time",{"class": "teaser__datetime"})
                        
                        date_art= datetime.strptime(time["datetime"], '%Y-%m-%d').date()
                        if date_debut <= date_art and date_art <= date_fin:
                            balise_a=bal.find("a",{"data-js":"teaser-link"})
                            links.append(journal+balise_a["href"])
                            print(links[-1])
                            dat.append(time["datetime"])
                            d=time.text
                            date.append(d.strip())
                        if date_art < date_debut :
                            limite = False
                            print(date_art)
                    except: pass
            t.sleep(0.5*rd.randint(1,4))

        ## création de la table de données

        df_mediapart = pd.DataFrame(columns = ["date", "datetime", "links"])
        print(len(links), len(dat), len(date))
        n=len(links)
        for i in range(n):  
            df_mediapart.append({"date" : date[i], "datetime" : dat[i] , "links" : links[i]}, ignore_index = True)
        
        df_mediapart["source"]=["mediapart"]*n

        self.data = df_mediapart

        return df_mediapart


    def contenu_articles(self) -> pd :
        '''
        retourne le titre , l'into et le contenu des articles dont leurs liens web 
        sont dans "data"
        '''
        df= self.lien_articles()
        article_links=list(df["links"]) # tous les liens enregistrés dans la table data
        T,I,C=[],[],[]    # titres, inros et contenus
        for l in article_links:
            print(l)
            url=l.strip()
            reponse=requests.get(url)
            if reponse.ok:
                soup=BeautifulSoup(reponse.text,'lxml')
                titre=" "
                try:
                    titre=soup.find("h1").text
                except:pass
                intro=" "
                try:
                    intro=soup.find("p",{"class":"news__heading__top__intro margin-top:500"}).text
                except:pass
                cont=" "
                try:
                    cont=soup.find("p",{"class":"dropcap-wrapper"}).text
                except :
                    pass    
                T.append(titre) 
                I.append(intro) 
                C.append(cont)

            t.sleep(0.2*rd.randint(1,3))  

        df["titres"]=T
        df["intros"]=I
        df["contenus"]=C
        self.data=df
        return df


    def selection_articles(self, mots_cle:list=[" "]) -> pd : 
        '''
        méthode de sélection des articles dans lesquels apparaissent mes mots de 
        la liste "mot_cle".
        '''
        df=self.contenu_articles()
        nrows=df.shape[0]
        index_to_keep=[]
        df1=df.copy(deep= True)

        for i in range(nrows):
            text= df["tites"][i] + df["intros"][i] +df ["contenus"] [i]
            text=text.lower()
            for elt in mots_cle :
                elt=str.lower(elt.strip())
                if elt in text :
                    index_to_keep.append(i)
                    break 
        all_index=range(nrows)         
        index_to_remove=[i for i in all_index if i not in set(index_to_keep)]
        df1.drop([index_to_remove])  

        self.data_selected=df1  

        return df1      


