import requests
from bs4 import BeautifulSoup
from datetime import datetime,date,timedelta
import time
import pandas as pd
import random as rd
import lxml

class Capital:
    '''
    cette clase permet d'accéder aux articles économiques du journal "capital" 
    qui ont été publiés à partir de "date_debut" jusqu'à "date_fin"

    les dates sont au format  AAAA-MM-JJ
    '''

    def __init__(self, date_debut:str, date_fin:str=str(date.today())) -> None:
        self.date_debut= date_debut
        self.date_fin=date_fin
        self.data=[]
        self.data_selected=[]

    def get_date_debut(self) -> str:
        return self.date_debut

    def get_date_fin(self) -> str:
        return self.date_fin
 


    def lien_articles_capital(self) -> pd :
        
       headers={'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
            'referer': 'https://www.google.com'}
       journal="https://www.capital.fr" 
       #url= "https://www.capital.fr/economie-politique?page="
       url = "https://www.capital.fr/entreprises-marches?page="

       date_inf= datetime.strptime(self.date_debut, '%Y-%m-%d').date()
       date_sup= datetime.strptime(self.date_fin, '%Y-%m-%d').date()
       df_capital = pd.DataFrame(columns = ["date", "datetime", "links"])

       page=1
       stop=False
       bonne_page_debut=False
       
       while not stop:
            links,dates,dat=[],[],[]

            lien=url+str(page)
            
            reponse=requests.get(lien,headers=headers)

            if reponse.ok:
                print(reponse)
                soup=BeautifulSoup(reponse.text,'lxml')
                balise_li=soup.find("main",{"class":"page-main"}).find_all("li",{"class": "cardList-item"}) ## chaque artice est contenu dans cette balise div
                print(bonne_page_debut)
                if not bonne_page_debut:
                    date_article=""
                    for li in balise_li:
                        try:
                            bal=li.find("article").find("span",{"class" :"card-date"}).find("time")
                            date_article=bal["datetime"][:10]
                            if   datetime.strptime(date_article, '%Y-%m-%d').date() <= date_sup:
                                bonne_page_debut=True
                                   
                        except:
                            pass
                    if bonne_page_debut:
                        page-=1        
                            
                else:

                    for li in balise_li:
                        
                        try:  
                            bal=li.find("article").find("span",{"class" :"card-date"}).find("time")
                            d_art= datetime.strptime(bal["datetime"][:10], '%Y-%m-%d').date()  
                            if d_art< date_inf:
                                stop = True
                            if d_art<= date_sup and d_art>= date_inf:
                                dat.append(str(d_art))
                                d=bal.text
                                dates.append(d.strip())
                                bal2=li.find("div",{"class":"card-title"}).find("a")
                                # nous allons rechercher tous les liens des articles payant; pour un article payant, l'élément class est une liste à la liste
                                # deux éléments
                                if not len(bal2["class"])==2: 
                                    # Ainsi, on n'ajoute un article que lorsque la liste qui contient la classe de la balise a est de 1
                                    links.append(journal+bal2["href"])    
                        except:
                            pass
                if bonne_page_debut and len(dat)!=0:
                    for i in range(len(links)): 
                        p=pd.DataFrame([[dates[i],dat[i],links[i]]], columns=["date", "datetime", "links"])  
                        df_capital=pd.concat(objs=[df_capital,p], ignore_index=True)

            time.sleep(0.08*rd.randint(1,3))
            page+=1
            print(bonne_page_debut, page)
       df_capital["source"]=["Capital"]*len(df_capital)     
       self.data=df_capital
       return df_capital 
   
# titres, intros et contenus
    def contenu_articles(self) -> pd :  
        '''
        retourne le titre , l'into et le contenu des articles dont leurs liens web 
        sont dans "data"
        '''
        headers={'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
            'referer': 'https://www.google.com'}
        df_contenu_articles= self.lien_articles_capital()
        article_links=list(df_contenu_articles["links"]) # tous les liens enregistrés dans la table data
        T,I,C=[],[],[]    # titres, intros et contenus

        for l in article_links:
             url=l.strip()
             reponse=requests.get(url,headers=headers)
             if reponse.ok:
                 soup=BeautifulSoup(reponse.text, features='lxml')
                 titre = ""
                 try:
                     titre=soup.find("h1", {"class":"article-title"}).text
                 except:pass
                 intro = ""
                 try :
                     intro = soup.find("h2", {"class":"article-intro"}).text
                 except : pass
                 cont = ""
                 try :
                     paragraphes=soup.find_all("p")
                     for p in paragraphes:
                        cont+=p.text
                 except:pass
                 T.append(titre) 
                 I.append(intro) 
                 C.append(cont)
             time.sleep(rd.randint(1,3))  

        df_contenu_articles["titres"]=T
        df_contenu_articles["intros"]=I
        df_contenu_articles["contenus"]=C
        self.data=df_contenu_articles
        return df_contenu_articles

                        

