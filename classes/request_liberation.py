
import requests
from bs4 import BeautifulSoup
from datetime import datetime,date,timedelta
import time
import pandas as pd
import random as rd

class Liberation:
    '''
    cette classe permet d'accéder aux articles économiques de "liberation" 
    qui ont été publiés à partir de "date_debut" jusqu'à "date_fin"

    les dates sont au format  AAAA-MM-JJ
    '''

    def __init__(self, date_debut:str, date_fin:str=str(date.today())) -> None:
        self.date_debut= date_debut
        self.date_fin=date_fin
        self.data=[]
        self.data_selected=[]

    def get_date_debut(self) -> str:
        return self.date_debut

    def get_date_fin(self) -> str:
        return self.date_fin

    def date_from_text(self,text:str) -> str:
        """
        retourne la date au format AAAA-MM-JJ à partir d'un texte
        exemple: 16 mars 2021 
        retourne 2021-03-16
        """ 
        mois={"janv.":"01","févr.":"02", "mars":"03" , "avr.":"04", "mai":"05", "juin":"06"
              , "juil.":"07", "août":"08", "sept.":"09", "oct.":"10", "nov.":"11", "déc.":"12" }
        #jours_special ={"1er": "01"}
        annee=""
        today_year=str(date.today())[:4] 
        
        l=text.split(" ")
        print(l,today_year)
        if len(l)<3:
            annee=today_year
        else:
            annee=l[2].strip()
        

        return annee+"-"+mois[l[1]]+"-"+l[0]   


    def lien_articles(self) -> pd :
        headers={'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
            'referer': 'https://www.google.com'}
        journal="https://www.liberation.fr"            
        url= "https://www.liberation.fr/economie/"
        date_inf= datetime.strptime(self.date_debut, '%Y-%m-%d').date()
        date_sup= datetime.strptime(self.date_fin, '%Y-%m-%d').date()
        df_liberation = pd.DataFrame(columns = ["date", "datetime", "links"])
        
        page=1
        stop=False
        bonne_page_debut=False

        while not stop:
            links,dates,dat=[],[],[]
            lien= url
            if page>1:
              lien=url+str(page)+"/"
            
            reponse=requests.get(lien,headers=headers)
            
            if reponse.ok:
                print(reponse)
                soup=BeautifulSoup(reponse.text,'lxml')
                balise_div=soup.find("div",{"class": "col-sm-md-12 col-lg-xl-8 left-article-section ie-flex-100-percent-sm layout-section"}) ## chaque artice est contenu dans cette balise div
                balise_articles= balise_div.find_all("article",{"class": "CustomContentListItem__Article-sc-2hcki7-0 euEONJ"})
                if not bonne_page_debut:
                    date_page=[]
                    for d in balise_articles:
                        try:
                            bal=d.find("div",{"class" :"sc-breuTD sc-iNWwEs ejPjna cZnlIv"})
                            bal=bal.text
                            print(bal) 
                            date_page.append(self.date_from_text(bal)) 
                                  

                        except:
                            pass
                             
                    for  elt in date_page:
                        print(date_page)
                        if elt.split("-")[2] == "1er":
                            elt = elt.replace("1er", "01")
                        if   datetime.strptime(elt, '%Y-%m-%d').date() <= date_sup:
                            bonne_page_debut=True
                    if bonne_page_debut:
                        page-=1        
                            
                else:

                    for div in balise_articles:
                        
                        try:  
                            bal=div.find("div",{"class" :"sc-breuTD sc-iNWwEs ejPjna cZnlIv"})
                            d_art= datetime.strptime(self.date_from_text(bal.text), '%Y-%m-%d').date()
                            if d_art< date_inf:
                                stop = True
                            if d_art<= date_sup and d_art>= date_inf + timedelta(days=-1):
                                dat.append(str(d_art))
                                d=bal.text
                                dates.append(d.strip())
                                bal2=div.find("a",{"class":"CustomContentListItem__Link-sc-2hcki7-2 kPzBgo"})
                                links.append(journal+bal2["href"])    
                        except:
                            pass
                if bonne_page_debut and len(dat)!=0:
                    for i in range(len(links)): 
                        article_date=datetime.strptime(dat[i], '%Y-%m-%d').date()
                        if article_date>= date_inf:
                            p=pd.DataFrame([[dates[i],dat[i],links[i]]], columns=["date", "datetime", "links"])  
                            df_liberation=pd.concat(objs=[df_liberation,p], ignore_index=True)

            time.sleep(0.08*rd.randint(1,3))
            page+=1
            print(page)
       

        df_liberation["source"]=["liberation"]*len(df_liberation)
        self.data = df_liberation

        return df_liberation


    def contenu_articles(self) -> pd :
        
        '''
        retourne le titre , l'into et le contenu des articles dont leurs liens web 
        sont dans "data"
        '''
        df= self.lien_articles()
        article_links=list(df["links"]) # tous les liens enregistrés dans la table data
        T,I,C=[],[],[]    # titres, intros et contenus
        for l in article_links:
            url=l.strip()
            reponse=requests.get(url)
            if reponse.ok:
                soup=BeautifulSoup(reponse.text,'lxml')
                titre=" "
                try:
                    titre=soup.find("h1").text
                except:pass
                intro=" "
                try:
                    intro=soup.find("span",{"class":"TypologyArticle__BlockSubHeadline-sc-1vro4tp-4 bHYlQv"}).text
                except:pass
                cont=" "
                try:
                    paragraphes=soup.find_all("p",{"class":"article_link"})
                    for p in paragraphes:
                        cont+=p.text
                except :
                    pass    
                T.append(titre) 
                I.append(intro) 
                C.append(cont)

            time.sleep(0.05*rd.randint(1,3))  

        df["titres"]=T
        df["intros"]=I
        df["contenus"]=C
        self.data=df
        return df


    def selection_articles(self, mots_cle:list=[" "]) -> pd :
        
        '''
        méthode de sélection des articles dans lesquels apparaissent mes mots de 
        la liste "mot_cle".
        '''
        
        df=self.contenu_articles()
        nrows=df.shape[0]
        index_to_keep=[]
        df1=df.copy(deep= True)

        for i in range(nrows):
            text= df["tites"][i] + df["intros"][i] +df ["contenus"] [i]
            text=text.lower()
            for elt in mots_cle :
                elt=str.lower(elt.strip())
                if elt in text :
                    index_to_keep.append(i)
                    break 
        all_index=range(nrows)         
        index_to_remove=[i for i in all_index if i not in set(index_to_keep)]
        df1.drop([index_to_remove])  

        self.data_selected=df1  

        return df1      


