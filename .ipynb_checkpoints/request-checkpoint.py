import requests
from bs4 import BeautifulSoup
import time

'''
headers={'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
            'referer': 'https://www.google.com'}
journal="https://www.mediapart.fr"            
url= "https://www.mediapart.fr/journal/economie?page="
date= "02/12/2022"
links=[]
for i in range(1,3):
    l=url+str(i)
    print(l)
    reponse=requests.get(l,headers=headers)
    print(reponse)
    if reponse.ok:
        soup=BeautifulSoup(reponse.text,'lxml')
        balise_a=soup.find_all("a")
        for bal in balise_a:
            try:
                if bal["data-js"]=="teaser-link" :
                    links.append(journal+bal["href"])
            except:
                pass        
    time.sleep(3)

with open("urls.txt","w") as file:
    for l in links:
       file.write(l+"\n")
'''

#obtenir les informations de chaque article


with open("urls.txt","r") as file_links:
    with open ("articles_médiapart.csv",'w') as f:
        f.write("titre, intro, contenu\n")      
        for row in file_links:
            url=row.strip()
            reponse=requests.get(url)
            if reponse.ok:
                soup=BeautifulSoup(reponse.text,'lxml')
                titre=""
                try:
                    titre=soup.find("h1").text
                except:pass
                intro=""
                try:
                    intro=soup.find("p",{"class":"news__heading__top__intro margin-top:500"}).text
                except:pass
                cont=""
                try:
                    cont=soup.find("p",{"class":"dropcap-wrapper"}).text
                except :
                    pass    
                f.write(titre+","+ intro+"," +cont+"\n")    
            time.sleep(2)