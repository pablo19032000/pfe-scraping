import pandas as pd
import numpy as np
from datetime import date

class Journal ():
    '''
    la classe qui nous permettra de récupérer les 
    articles d'un journal spécifique
    '''

    def __init__(self,titre:str) -> None:

        self.titre=titre

    def name (self):
        return self.titre

    def articles (self, date_limite: str) -> pd.DataFrame:
        '''
        cette méthode renvoi tous les articles publiés à partir de
        (date_limite)
        ''' 
