# Dans cette feuille, nous allons collecter tous les articles traitant du pib et effectuer le LDA afin de ressortir une 
# analyse de sentiment qui en découlera.


import copy 
from datetime import datetime,date,timedelta

# packages for LDA analysis

import pandas as pd
import os
import re
import matplotlib.pyplot as plt
from pprint import pprint

# gensim
import gensim
from gensim.utils import simple_preprocess
import gensim.corpora as corpora
from datetime import datetime
#NLTK stop words

import nltk 
from spacy.lang.fr.stop_words import STOP_WORDS
from wordcloud import WordCloud

#visualization

import pyLDAvis.gensim
import pickle 
import pyLDAvis


# importation de spacy
import spacy
nlp = spacy.load('en_core_web_md')

# Nous allons faire la sélection de ces articles qui incluent le pib

# Importation de la base totale
papers_pib = pd.read_csv("Analyses\Groupes_data_pib\groupe_2_articles_pib.csv", sep="|", encoding = "utf-8")



data_senti =pd.read_csv("Analyses\data_sentiments.csv", sep="|", encoding = "utf-8")

data_senti.columns
data_senti["sentiment_contenu"]

len(data_senti)


# nous allons afficher la repartition de ces scores

from scipy.stats import gaussian_kde
import numpy as np


density_empiric = gaussian_kde(data_senti["sentiment_contenu"])
X = np.linspace(-0.5, 0.5 , 1000 )


plt.plot(X, density_empiric(X))
plt.show()



# nous allons représenter les polarités des articles de la période 2020 qui s'avère être assez négatif

#essayer les scores moyens par trimestre avec représentation des scores moyens ( pib )

# kmeans sur tout avec les scores et les vecteurs de mêmes



data_covid_2020 = data_senti.loc[data_senti["date.time"]]

#  nous allons aussi effectuer les polarités moyennes par trimestre



data_senti.head()


from traitement_articles import col_indicateur

"""
papers_pib = col_indicateur(papers , ["pib" , "produit intérieur brut"])
papers_pib = papers_pib.loc[papers_pib.PIB == 1, ["links","Allfiltered_intro","Allfiltered_titre","Allfiltered_contenu"] ]
#Nous considérons les articles avec 
papers_pib.shape
papers_pib["Allfiltered_contenu"][44225]

sum(papers_pib.PIB)
# Nous avons retenu 1202 articles qui parlent du PIB.
# Nous allons effectuer la LDA sur ceux - ci"""

papers_pib['paper_text_processed'] = papers_pib['contenu'].map(lambda x: re.sub('[,\.!?]', ' ', x))


# Convert the titles to lowercase
papers_pib['paper_text_processed'] = papers_pib['paper_text_processed'].map(lambda x: x.lower())

# We will use the word cloud in order to displays all the main words presents in this full text

# Import the wordcloud library

# Join the different processed titles together en une seule chaine de caractères

long_string = ','.join(list(papers_pib['paper_text_processed'].values))
# Create a WordCloud object
wordcloud = WordCloud(background_color="white", max_words=5000, contour_width=3, contour_color='steelblue')

# Generate a word cloud
wordcloud.generate(long_string)

# Visualize the word cloud
wordcloud.to_image()
plt.imshow(wordcloud)
plt.show()

# l'image montre que les mots les plus présent sont les adjectifs que nous devons 
# retirer de la liste de mots.

# La liste des mots à retirer est donc

stop_words=set(STOP_WORDS)

# on va essayer de l'enlever
#deselect_stop_words = ['n\'', 'ne','pas','plus','personne','aucun','ni','aucune','rien',"n'"]
"""for w in deselect_stop_words:
    if w in stop_words:
        stop_words.remove(w)"""
        
len(stop_words)
#Dans le journal Capital, il y a certains mots qui font partie des signatures du journal
# ce sont : groupe Vivendi, Prisma Média, media groupe, réservé lire aussi, droits réservés
# lire aussi 

# Nous allons les inclure dans les stop word afin d'affiner l'analyse


stop_words.update(["Prisma", "Média", "groupe" ,"Vivendi", "media", "groupe","réservé", "lire aussi", "droits", "reservés","prisma", 'vivendi',
                   "reserves", "euros", "dollars", "millions", "milliards", "aof", 'qu', "lire","ans","france","ete","capital", "mois", "janvier", "fevrier",
                   "mars", "avril", "mai", "juin", "juillet", "aout", "septembre", "octobre", "novembre","decembre", "moment","annonce","pourcent"])
len(stop_words)





def sent_to_words(sentences):
    for sentence in sentences:
        # deacc=True removes punctuations
        yield(gensim.utils.simple_preprocess(str(sentence), deacc=True))

def remove_stopwords(texts):
    return [[word for word in simple_preprocess(str(doc)) 
             if word not in stop_words] for doc in texts]

# data contient une liste de tous les tokens des mots contenus dans tous les textes

data = papers_pib.paper_text_processed.values.tolist()
data_words = list(sent_to_words(data))# remove stop words

# data_word est en fait une liste de taille le nombre d'articles et qui contient les tokens de chaque article
# nous ne voulons que garder les adj, les verb, les nom et les adv
# Nous allons enlever les stops words
data_words = remove_stopwords(data_words)





# On ne gardera aussi que les lemmes de ces sujets


def lemmatization(texts, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV']):
    texts_out = []
    for sent in texts:
        doc = nlp(" ".join(sent)) 
        texts_out.append([token.lemma_ for token in doc if token.pos_ in allowed_postags])
    return texts_out


data_lemmatized = lemmatization(data_words )
type(data_lemmatized[1][1])

long_string = ""
for i in range(len(data_lemmatized)):
    long_string = long_string + ','.join(data_lemmatized[i])
long_string
# Create a WordCloud object
wordcloud = WordCloud(background_color="white", max_words=5000, contour_width=3, contour_color='steelblue')

# Generate a word cloud
wordcloud.generate(long_string)

# Visualize the word cloud
wordcloud.to_image()
plt.imshow(wordcloud)
plt.show()


# Create Dictionary
# Permet de créer un ensemble clé-valeur contenant chauqe mots de data words
id2word = corpora.Dictionary(data_words)
# Create Corpus
texts = data_words# Term Document Frequency
corpus = [id2word.doc2bow(text) for text in texts]# View
print(corpus[:1][0][:30])

# Model training
# number of topics
num_topics = 2

# Build LDA model
lda_model_1 = gensim.models.LdaMulticore(corpus=corpus,
                                       id2word=id2word,
                                       num_topics=num_topics,
                                       alpha = 0.5, 
                                       eta=0.5,
                                       passes = 10, # le nombre de fois que tout le modèle est entrainé
                                       chunksize = 100) # le nombre de documents utilisés dans une étape d'apprentissage
                                
                                


# Print the Keyword in the 10 topics
pprint(lda_model_1.print_topics())
doc_lda = lda_model_1[corpus]

# Nous allons à présent évaluer le modèle LDA à l'aide de la métrique nommée cohérence

# Visualize the topics
pyLDAvis.enable_notebook()
LDAvis_data_filepath = os.path.join('.NLP_PIB_modeling_groupe2_'+str(num_topics))# # this is a bit time consuming - make the if statement True
# # if you want to execute visualization prep yourself

LDAvis_data_filepath
if 1 == 1:
    LDAvis_prepared = pyLDAvis.gensim.prepare(lda_model_1, corpus, id2word)
    with open(LDAvis_data_filepath, 'wb') as f:
        pickle.dump(LDAvis_prepared, f)
# load the pre-prepared pyLDAvis data from disk
        
        
        
with open(LDAvis_data_filepath, 'rb') as f:
    LDAvis_prepared = pickle.load(f)
    pyLDAvis.save_html(LDAvis_prepared, '.NLP_PIB_modeling_groupe2_'+ str(num_topics) +'.html')
    

from gensim.models import CoherenceModel

# Compute Coherence Score
coherence_model_lda = CoherenceModel(model=lda_model, texts=data_lemmatized, dictionary=id2word, coherence='c_v')
coherence_lda = coherence_model_lda.get_coherence()
print('Coherence Score: ', coherence_lda)


 # premier test avec text blob
 
 # l'articke suivant présente des caractéristiques poositives*
 
from textblob import TextBlob
from textblob_fr import PatternAnalyzer, PatternTagger


papers_text = pd.read_csv("Analyses\Groupes_data_pib\groupe_1_articles_pib.csv", sep="|", encoding = "utf-8")

# cet texte ressort un sentiment positif
papers_text.loc[18 , "contenu"]

import numpy as np
# Création d'un objet TextBlob pour l'article
papers_text["polarity"] = np.zeros(len(papers_text))
papers_text
# nous allons polariser chaque article de la table
for i in range( len(papers_text)):
    papers_text.loc[i , "polarity"] = TextBlob( papers_text.loc[i , "contenu"], pos_tagger = PatternTagger(), analyzer = PatternAnalyzer()).sentiment[1]
    
len(papers_text)

# Analyse du sentiment de l'article
sentiment = blob_1.sentiment.polarity
sentiment
# Affichage du sentiment
if sentiment > 0:
    print("L'article est positif.")
elif sentiment < 0:
    print("L'article est négatif.")
else:
    print("L'article est neutre.")
    
    
TextBlob( "  ").sentiment.polarity