import pandas as pd
import os
import re
import matplotlib.pyplot as plt
from pprint import pprint

# gensim
import gensim
from gensim.utils import simple_preprocess
import gensim.corpora as corpora
from datetime import datetime
#NLTK stop words

import nltk 
from spacy.lang.fr.stop_words import STOP_WORDS
from wordcloud import WordCloud

#visualization

import pyLDAvis.gensim
import pickle 
import pyLDAvis


# importation de spacy
import spacy
nlp = spacy.load('en_core_web_md')


# Visualize the topics



# Read the CSV file into a pandas DataFrame
papers = pd.read_csv("Analyses\Echo_monde_capital_2020_2022.csv", sep="|", encoding = "utf-8")


# we will drop all column but the title,  the description and the full text
# Remove the columns
papers = papers.drop(papers.columns[[0, 2, 3]], axis=1)
# Print out the first rows of papers


# we will treat only the content column

papers['paper_text_processed'] = papers['contenu'].map(lambda x: re.sub('[,\.!?]', ' ', x))



# Convert the titles to lowercase
papers['paper_text_processed'] = papers['paper_text_processed'].map(lambda x: x.lower())

# We will use the word cloud in order to displays all the main words presents in this full text

# Import the wordcloud library

# Join the different processed titles together en une seule chaine de caractères

long_string = ','.join(list(papers['paper_text_processed'][1:5].values))
# Create a WordCloud object
wordcloud = WordCloud(background_color="white", max_words=5000, contour_width=3, contour_color='steelblue')

# Generate a word cloud
wordcloud.generate(long_string)

# Visualize the word cloud
wordcloud.to_image()
plt.imshow(wordcloud)
plt.show()

# l'image montre que les mots les plus présent sont les adjectifs que nous devons 
# retirer de la liste de mots.

# La liste des mots à retirer est donc

stop_words=set(STOP_WORDS)

# on va essayer de l'enlever
#deselect_stop_words = ['n\'', 'ne','pas','plus','personne','aucun','ni','aucune','rien',"n'"]
"""for w in deselect_stop_words:
    if w in stop_words:
        stop_words.remove(w)"""
        
len(stop_words)
#Dans le journal Capital, il y a certains mots qui font partie des signatures du journal
# ce sont : groupe Vivendi, Prisma Média, media groupe, réservé lire aussi, droits réservés
# lire aussi 

# Nous allons les inclure dans les stop word afin d'affiner l'analyse


stop_words.update(["Prisma", "Média", "groupe" ,"Vivendi", "media", "groupe","réservé", "lire aussi", "droits", "reservés","prisma", 'vivendi',
                   "reserves", "euros", "dollars", "millions", "milliards", "aof", 'qu', "lire","ans","france","ete","capital", "mois", "janvier", "fevrier",
                   "mars", "avril", "mai", "juin", "juillet", "aout", "septembre", "octobre", "novembre","decembre", "moment","annonce"])
len(stop_words)







def sent_to_words(sentences):
    for sentence in sentences:
        # deacc=True removes punctuations
        yield(gensim.utils.simple_preprocess(str(sentence), deacc=True))

def remove_stopwords(texts):
    return [[word for word in simple_preprocess(str(doc)) 
             if word not in stop_words] for doc in texts]

# data contient une liste de tous les tokens des mots contenus dans tous les textes

data = papers.paper_text_processed.values.tolist()
data_words = list(sent_to_words(data))# remove stop words

# data_word est en fait une liste de taille le nombre d'articles et qui contient les tokens de chaque article
# nous ne voulons que garder les adj, les verb, les nom et les adv
data_words[1000]
# Nous allons enlever les stops words
data_words = remove_stopwords(data_words)

# On ne gardera aussi que les lemmes de ces sujets


def lemmatization(texts, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV']):
    texts_out = []
    for sent in texts:
        doc = nlp(" ".join(sent)) 
        texts_out.append([token.lemma_ for token in doc if token.pos_ in allowed_postags])
    return texts_out

data_lemmatized = lemmatization(data_words )
data_lemmatized


# Create Dictionary
# Permet de créer un ensemble clé-valeur contenant chauqe mots de data words
id2word = corpora.Dictionary(data_words)
# Create Corpus
texts = data_words# Term Document Frequency
corpus = [id2word.doc2bow(text) for text in texts]# View
print(corpus[:1][0][:30])

# Model training
# number of topics
num_topics = 10

# Build LDA model
lda_model = gensim.models.LdaMulticore(corpus=corpus,
                                       id2word=id2word,
                                       num_topics=num_topics,
                                       alpha = 0.5, 
                                       eta=0.5,
                                       passes = 10, # le nombre de fois que tout le modèle est entrainé
                                       chunksize = 100) # le nombre de documents utilisés dans une étape d'apprentissage
                                       
                                    

#lda_model.print_topics(num_topics = num_topics , num_words = 5)


# Print the Keyword in the 10 topics
pprint(lda_model.print_topics())
doc_lda = lda_model[corpus]

# Nous allons à présent évaluer le modèle LDA à l'aide de la métrique nommée cohérence


from gensim.models import CoherenceModel

# Compute Coherence Score
coherence_model_lda = CoherenceModel(model=lda_model, texts=data_lemmatized, dictionary=id2word, coherence='c_v')
coherence_lda = coherence_model_lda.get_coherence()
print('Coherence Score: ', coherence_lda)


lda_model.show_topics(formatted =False)

# Nous allons exporter les résultats et les interprèter 


