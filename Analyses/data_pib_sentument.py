import pandas as pd
import numpy as np
import spacy
from textblob import Blobber
from textblob_fr import PatternTagger, PatternAnalyzer
tb = Blobber(pos_tagger=PatternTagger(), analyzer=PatternAnalyzer())
from traitement_articles import select_date, col_indicateur
import copy
from datetime import datetime,date,timedelta
from spacy.lang.fr.stop_words import STOP_WORDS
import matplotlib.pyplot as plt
from Analyses.analyse import regrouper_tokens, sentiment
from Analyses.netoyage import extract_lemmas

nlp = spacy.load("fr_core_news_md")
#Importation de la base de donnée filtrée data_pib

data_pib = pd.read_csv("Analyses\Data_pib.csv", sep="|", encoding="utf-8")


#Initialisation des stop words à enlever 
stop_words=set(STOP_WORDS)

deselect_stop_words = ['n\'', 'ne','pas','plus','personne','aucun','ni','aucune','rien',"n'"]
for w in deselect_stop_words:
    if w in stop_words:
        stop_words.remove(w)

#Suppression des stop word dans la base de départ data_pib

#fonction qui supprime les stop word
def remove_stop_w(text):
    doc = nlp(text)
    text_filtre = [token.text for token in doc if not ((token.text in stop_words) or (len(token.text) == 1))]
    text_final = " ".join(text_filtre)
    return text_final

data_pib_filtre =  pd.DataFrame() 
data_pib_filtre["datetime"] = data_pib["datetime"]
data_pib_filtre["links"] = data_pib["links"]
data_pib_filtre["source"] = data_pib["source"]



#création des colonne titre, intro et contenu dont on a supprimé les stop words
data_pib_filtre["titre_filtre"] = data_pib["titre"].astype(str).apply(remove_stop_w)
data_pib_filtre["intro_filtre"] = data_pib["intro"].astype(str).apply(remove_stop_w)
data_pib_filtre["contenu_filtre"] = data_pib["contenu"].astype(str).apply(remove_stop_w)

data_pib_filtre["intro_filtre"][0]

data_pib_filtre.to_csv("Analyses\Data_pib_filtre.csv", sep="|", encoding="utf-8")

#création de la colonne sentiment 

data_pib_filtre["sentiment_contenu"] = data_pib_filtre["contenu_filtre"].apply(sentiment)

data_pib_filtre.to_csv("Analyses\Data_pib_filtre_senti.csv", sep="|", encoding="utf-8")
