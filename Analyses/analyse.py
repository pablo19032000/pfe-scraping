import pandas as pd
import numpy as np
import spacy
from textblob import Blobber
from textblob_fr import PatternTagger, PatternAnalyzer
tb = Blobber(pos_tagger=PatternTagger(), analyzer=PatternAnalyzer())
from traitement_articles import select_date, col_indicateur
import copy
from datetime import datetime,date,timedelta
from spacy.lang.fr.stop_words import STOP_WORDS
import matplotlib.pyplot as plt

df_lemmas = pd.read_csv("Analyses\data_lemmas.csv", sep="|", encoding = "utf-8")

## on retient les articles du premier trimestre 2020

data_jan_mars_2020=select_date(date_debut= "2020-01-01",date_fin="2020-03-31",base=df_lemmas)

## on enlève les stopwords de nos tokens qui ne sont pas pertinent dans l'analyse de sentiments
stop_words=set(STOP_WORDS)

deselect_stop_words = ['n\'', 'ne','pas','plus','personne','aucun','ni','aucune','rien',"n'"]
for w in deselect_stop_words:
    if w in stop_words:
        stop_words.remove(w)
    
def regrouper_tokens(data):
    l_var=["intro_lemmas","titre_lemmas","Contenu_lemmas"]
    filtre=["Allfiltered_intro","Allfiltered_titre","Allfiltered_contenu"]
    for var,var_filtre in zip(l_var,filtre):
        Allfilteredtext=[]
        for list_word in data[var]:
            list_word=list_word[1:len(list_word)-1]
            list_word=list_word.replace("'","")
            list_word=list_word.replace(",","")
            list_word= list_word.split(" ")
            filteredtext = [w for w in list_word if not ((w in stop_words) or (len(w) == 1))]
            Allfilteredtext.append(' '.join(filteredtext)) 
            
        data[var_filtre]=  Allfilteredtext

    return data

data_jan_mars_2020=regrouper_tokens(data_jan_mars_2020)
data_sentiments=regrouper_tokens(df_lemmas)



## fonction pour obtenir le sentiment d'un texte

def sentiment(tex):
    vs= tb(tex).sentiment[0]
    return vs

text= " heureux triste "  
print(sentiment(text))

## création de la colonne des sentiments

data_jan_mars_2020["sentiment"]= data_jan_mars_2020["Allfiltered_contenu"].apply(sentiment)

#data_sentiments["sentiment_intro"]= data_sentiments["Allfiltered_intro"].apply(sentiment)
#data_sentiments["sentiment_titre"]= data_sentiments["Allfiltered_titre"].apply(sentiment)
#data_sentiments["sentiment_contenu"]= data_sentiments["Allfiltered_contenu"].apply(sentiment)

## exportation de data_sentiments total

###data_sentiments.to_csv("Analyses\data_sentiments.csv", sep="|", encoding = "utf-8", index=False)

#print(data_sentiments.loc[0:10,"sentiment_intro"])
#petite visualisation des sentiments
plt.plot(range(len(data_jan_mars_2020)),data_jan_mars_2020["sentiment"],".r")
plt.title("Variation des sentiments des articles")

plt.show()

sum(data_jan_mars_2020["sentiment"]>0.005)
sum((data_jan_mars_2020["sentiment"]<=0.005) & (data_jan_mars_2020["sentiment"]>=-0.005))
sum(data_jan_mars_2020["sentiment"]<-0.005)



## réalisons les classes de sentiment

df_sentiments = pd.read_csv("Analyses\data_sentiments.csv", sep="|", encoding = "utf-8") 
df_total = pd.read_csv("Analyses\Echo_monde_capital_2020_2022.csv", sep="|", encoding = "utf-8") 

## on sélectionne le premier trimestre 2020 pour les premières analyses

df_janv_mars_2020= select_date(date_debut= "2020-01-01",date_fin="2020-03-31",base=df_sentiments)



## On rajoute la colonne PIB
lemme_pib=[" pib "," produit intérieur brut "," produit national brut ", " pnb "," bien-être économique ", " croissance économique "]
df_janv_mars_2020 = col_indicateur(df_total , lemme_pib )

count = df_janv_mars_2020['PIB'].value_counts()   ## 0:4700  1:154

df=df_janv_mars_2020[["Allfiltered_intro","Allfiltered_titre","Allfiltered_contenu","PIB"]]
print(df.iloc[0,2])

## affichons les sentiments des articles sélectionnées (séparés en titres , intro , contenu)

## chargeons le bon type des colonnes concernées
df_janv_mars_2020["sentiment_intro"]= float(df_janv_mars_2020["sentiment_intro"])
df_janv_mars_2020["sentiment_titre"]= float(df_janv_mars_2020["sentiment_titre"])
df_janv_mars_2020["sentiment_contenu"]= float(df_janv_mars_2020["sentiment_contenu"])

y=[0.5*i for i in range(int(count[1]))]

fig, (ax1, ax2, ax3) = plt.subplots(3, 1)

fig.suptitle("Sentiment d'articles évoquant le PIB")

ax1.plot(df_janv_mars_2020.loc[df_janv_mars_2020['PIB']==1,"sentiment_intro"] ,y,".b", label="intro")
#ax1.set_title("sentiment intro")
ax1.legend()

ax2.plot(df_janv_mars_2020.loc[df_janv_mars_2020['PIB']==1,"sentiment_titre"],y,".r", label="titre")
#ax2.set_title("sentiment titre")
ax2.legend()

ax3.plot(df_janv_mars_2020.loc[df_janv_mars_2020['PIB']==1,"sentiment_contenu"],y,".k", label="contenu")
ax3.legend()



plt.show()

