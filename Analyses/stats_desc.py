import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from tqdm import tqdm

## importation de data_sentiments

df=pd.read_csv("Analyses\data_sentiments.csv", sep="|", encoding = "utf-8")

# Convertir la colonne date en type datetime
df['datetime'] = pd.to_datetime(df['datetime'])

# Agréger les données trimestriellement
df_quarterly = df[["datetime","links"]].resample('Q', on='datetime').count()

# Représenter l'évolution trimestrielle
df_quarterly.columns=["volume"]
df_quarterly.plot(kind='line', title="Evolution du volume trimestriel d'articles")
plt.ylim(0,5000)
plt.show()

# Agréger les données trimestriellement par source de journal
df_quarterly_by_journal = df[["datetime","source"]].groupby('source').resample('Q', on='datetime').count()
df_quarterly_by_journal.columns=["volume"]
# Représenter l'évolution trimestrielle par catégorie de journal
df_quarterly_by_journal.unstack(level=0)['volume'].plot(kind='line',title="Evolution du volume trimestriel d'articles par source")
plt.ylim(0,4000)
plt.show()


