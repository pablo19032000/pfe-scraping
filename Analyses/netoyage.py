
'''Avant importation des packages, nous allons tout d'abord installer spaCy à l'aide la commande "pip install spaCy"
 puis charger "python -m spacy download fr_core_news_md"
'''
'''pour la lemmatisation nous allons juste garder : les noms, adjectifs, adverbes, 
'''
import pandas as pd
import numpy as np
import spacy

df_lemmas = pd.read_csv("Analyses\data_lemmas.csv", sep="|", encoding = "utf-8")
df_lemmas["intro_lemmas"][1]

#charger le modèle de traitement de chaine de caractère en fraçais en mémoire

nlp = spacy.load("fr_core_news_md")
#importation de la base totale


df_total = pd.read_csv("Analyses\Echo_monde_capital_2020_2022.csv", sep="|", encoding = "utf-8")





#définition de la fonction qui extrait les lemma des tokens d'un text
def extract_lemmas(text):
    doc = nlp(str(text))
    lemmas = [token.lemma_ for token in doc if token.pos_ in ['NOUN', 'ADJ', 'ADV']]
    return lemmas

df_lemmas = pd.DataFrame()
df_lemmas["datetime"] = df_total["datetime"]
df_lemmas["links"] = df_total["links"]
df_lemmas["source"] = df_total["source"]

# lemmes des tokens d'intros
into_lemmas = df_total['intros'].apply(extract_lemmas)
df_lemmas["into_lemmas"] = into_lemmas

# lemmes des tokens de titres

titres_lemmas = df_total['titres'].apply(extract_lemmas)
df_lemmas["titre_lemmas"] = titres_lemmas

# lemmes des tokens de contenu
Contenu_lemmas = df_total['Contenu'].apply(extract_lemmas)
df_lemmas["Contenu_lemmas"] = Contenu_lemmas


# exportation de la base obtenue
df_lemmas.to_csv("Analyses\data_lemmas.csv", sep="|", encoding = "utf-8", index=False)


# table des sentiments
df_sentiments = pd.read_csv("Analyses\data_sentiments.csv", sep="|", encoding = "utf-8") 
df_sentiments["titres"]=df_total["titres"]
df_sentiments["intros"]=df_total["intros"]
df_sentiments["Contenu"]=df_total["Contenu"]
#exportation table des sentiments
df_sentiments.to_csv("Analyses\data_sentiments.csv", sep="|", encoding = "utf-8", index=False)



## les lemmes des différents mots liées au PIB
text= "le Produit intérieur brut (PIB),le Produit intérieur brut national (PIB national), le Produit national brut (PNB),la Croissance économique,  l'Indicateur de croissance économique, la Valeur ajoutée totale, la Mesure du bien-être économique"
lemmes_PIB = extract_lemmas(text)

for elt in lemmes_PIB:
    print(elt)