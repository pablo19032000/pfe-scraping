import pandas as pd
import nltk
from nltk.sentiment.vader import SentimentIntensityAnalyzer
import spacy
from textblob import Blobber
from textblob_fr import PatternTagger, PatternAnalyzer
tb = Blobber(pos_tagger=PatternTagger(), analyzer=PatternAnalyzer())
from traitement_articles import select_date, col_indicateur
from textblob import TextBlob
from textblob_fr import PatternAnalyzer, PatternTagger

df_sentiments = pd.read_csv("Analyses\data_sentiments.csv", sep="|", encoding = "utf-8") 
data_jan_mars_2020=select_date(date_debut= "2020-01-01",date_fin="2020-03-31",base=df_sentiments)
data_PIB= col_indicateur(data_jan_mars_2020,[" pib ", " pnb "," produit intérieur brut "," produit national brut "])

data_select_pib=data_PIB.loc[data_PIB["PIB"] == 1]
data_select_pib.to_csv("Analyses\data_select_pib.csv", sep="|", encoding = "utf-8", index=False)

def sentiment(tex):   
    vs= tb(tex).sentiment[0]
    return vs

texte_liste = [" je suis malheureux ", "je ne suis pas malheureux","l'echec pour mieux se relever",'Le film était génial', 'La nourriture était dégueulasse', 'Le service est un désastre' ,'Le service est désastreux']

sentiments = []
sentiments_={}

import numpy as np

for texte in texte_liste :
    sentiments_[texte]=sentiment(texte)
    

print(sentiments_)

data_pib_senti = pd.read_csv("Analyses\Data_Pib.csv", sep="|", encoding = "utf-8")

data_pib_senti["polarity"] = np.zeros(len(data_pib_senti))

# nous allons représenter 
list_senti = []
# nous allons polariser chaque article de la table
for i in range( len(data_pib_senti)):
#    data_pib_senti.loc[i , "polarity"] = TextBlob( data_pib_senti.loc[i , "contenu"], pos_tagger = PatternTagger(), analyzer = PatternAnalyzer()).sentiment[1]
    list_senti.append (  TextBlob( data_pib_senti.loc[i , "contenu"], pos_tagger = PatternTagger(), analyzer = PatternAnalyzer()).sentiment[1])
    
TextBlob( data_pib_senti.loc[88 , "contenu"], pos_tagger = PatternTagger(), analyzer = PatternAnalyzer()).sentiment[1]
