# ce code nous permettra d'obtenir tous les liens de l'article capital
import requests
import bs4
from bs4 import BeautifulSoup
import time
import pandas as pd
from urllib.error import HTTPError
from urllib.request import urlopen
from datetime import date
from datetime import datetime
from datetime import timedelta
import requests  # the following imports are common web scraping bundle

from urllib.error import HTTPError
from collections import defaultdict
from urllib.error import URLError
from tqdm import tqdm
import pandas as pd
import re
from requests.auth import HTTPBasicAuth
import urllib3
from pyquery import PyQuery
import webbrowser
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import lxml


driver = webdriver.Firefox()

# on sélectionne le lien pour avoir accès à tous les articles d'économie
headers={'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
            'referer': 'https://www.google.com'}
url= "https://www.lesechos.fr/industrie-services?page=2"
journal="https://www.lesechos.fr" 

#nous allons sélectionner tous les articles de la page donnée 

acces_page = requests.get(url, headers= headers)

# le lxml permet d'accéder au contenu html et au contenu xml
scrap = BeautifulSoup(acces_page.text , "lxml")
all_articles = scrap.find_all("div",{"class": "sc-1vhx99f-0 bxJLXB"})

# pour chaque article se troivant dans cette liste, je vais inspecter le type de son abonnement
type_ab = []
liste_non_abo = []
k  = 0
for article in all_articles:
    try :
        # on va savoir si l'article est payant ou non
    # on va rechercher la balise du titre

        balise_du_titre = article.find("h3")
        balise_abonne = balise_du_titre.find_all("span",{'class':"sc-aamjrj-0 cdOJus"})
        if len(balise_abonne) == 0:
            la_carte_du_titre = article.find("article").find("a",{"class":"sc-1560xb1-0 loUvLB sc-iphbbg-1 bMoVlm"})["href"]
            liste_non_abo.append(journal+ la_carte_du_titre)

        
        #titre_art = article.find("h3", {"class":"sc-14kwckt-6 sc-1ohdft1-0 jOjJTh eUIWcH sc-4cuy4z-0 iruOfP"})
        #print (article.prettify())
    except:
        pass
    """if len(la_carte_du_titre) ==1 :
        balise_du_titre = la_carte_du_titre.findAll("h3")
        print(len(balise_du_titre))"""
    #type_ab.append(len(la_carte_du_titre))

# Nous écrivons la fonction qui permet de receuillir toutes les info des echos.
liste_non_abo

def get_contenu_link_lesechos(url):
    driver.get(url)
    # on crée la liste de trois éléments qui sont le titre, la description et le contenu total
    full_content = list(range(3))
    
    # Nous récupérons le titre de l'article
    try:
        titre_article = driver.find_elements(By.TAG_NAME , "h1")
        titre_total = ''
        for title in titre_article:
            titre_total += title.text.strip()
                    
        full_content[0] = titre_total
    except :
        pass
    
    
    # Ici, nous récupérons la description
    try:
        description = driver.find_elements(By.CSS_SELECTOR, ".sc-14kwckt-6.sc-1ohdft1-0.fTlaQZ.gFCllC")
        description_totale = ''
        for desc in description:
            description_totale += desc.text.strip()
        full_content[1] = description_totale
    except :
        pass
    
        
    # ici, nous récupérons les paragraphes et nous les concatenons
    try:
        paragraphaphes = driver.find_elements(By.CSS_SELECTOR, "[class='sc-14kwckt-6 gPHWRV']")
        text_total = ''
        for paragraphe in paragraphaphes:
            text_total += paragraphe.text.strip()
        full_content[2] = text_total
    except:
        pass
    
    return full_content

# Dès à présent, nous allons sélectionner tous les articles des echos pendant toute cette durée
df_echos_2020 = pd.read_csv(r'C:\Users\yanni\OneDrive\Documents\ENSAI\PFE IFRS9\web_scrapping_part\pfe-scraping\Liens_les_echos_2020.csv', delimiter= '|', encoding='utf-8')



df_full_content_entreprise = pd.DataFrame(columns=["date", "links","source", "titre", "description", "contenu"])
df_full_content_entreprise["links"] = df_echos_2020["links"]
df_full_content_entreprise["source"] = df_echos_2020["source"]
df_full_content_entreprise["date"] = df_echos_2020["datetime"]


T,D,C = [],[],[]

for article in df_full_content_entreprise["links"].tolist() :
    try:
        contenu_article = get_contenu_link_lesechos(article)
        T.append(contenu_article[0])
        D.append(contenu_article[1])
        C.append(contenu_article[2])
    except:
        pass
    
    
df_full_content_entreprise["titre"] = T
df_full_content_entreprise["description"] = D
df_full_content_entreprise["contenu"]= C

df_full_content_entreprise.to_csv("LesEchos_2020.csv", sep='|' , encoding="UTF-8")


